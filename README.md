# Petnashki

## Implementation conditions

The game must have a menu and gameplay. Break them into different scenes.
The following list of global services must also be implemented in the game: AudioService, AnimationService, SceneLoadService, UIService.

Menu
You can use MonoBehaviour on the menu scene.
The stage should have a start button and a settings button. The button should bounce when clicked.
The settings must be made as a menu. In them, you can disable and enable sound and animation.

GamePlay
MonoBehaviour cannot be used on the game scene, except for Installers and SceneContext. Zenject has tools that allow you to do this.
You can also open the settings menu from the game (You can use MonoBehaviour for the button).
After the victory, there should be a popup from which you can exit the menu or start over.
When you click on a tag element, it should bounce.
The game grid can be made 3 by 3 to make it easier to complete the game.